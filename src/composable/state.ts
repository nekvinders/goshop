import { Cart, UserDetails } from '@/utils/interfaces'
import { ref } from '@vue/reactivity'

export const openModal = ref(false)
export const userCart = ref(new Cart([]))
export const userWishlist = ref(new Cart([]))
export const dialog = ref(false)
export const userDetails = ref({
  address: {
    line1: 'Godrej Eternia, 7th Floor,Old Mumbai Pune Highway',
    line2: 'Shivaji Nagar, Wakdewadi Road',
    country: 'India',
    city: 'Pune',
    state: 'Maharashtra',
    pin: ' 411005',
  },
} as UserDetails)

export const messageText = ref('')
export const messageState = ref('success')
export const showMessage = ref(false)

export const showSuccess = (message: string) => {
  messageText.value = message
  messageState.value = 'success'
  showMessage.value = true
  setTimeout(() => (showMessage.value = false), 3000)
}

export const showError = (message: string) => {
  messageText.value = message
  messageState.value = 'error'
  showMessage.value = true
  setTimeout(() => (showMessage.value = false), 3000)
}
