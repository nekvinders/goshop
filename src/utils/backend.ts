import axios, { AxiosRequestConfig } from 'axios'

const instance = axios.create({
  baseURL: 'http://35.225.250.72:8080/remote',
  params: {} // do not remove this, its added to add params later in the config
})

export async function getData(endPoint:string) {
  return await instance.get(endPoint)
}

export async function postData(endPoint:string, data:any) {
  return await instance.post(endPoint, data)
}

export async function deleteData(endPoint:string) {
  return await instance.delete(endPoint)
}

export async function postDataWithFile(endPoint:string, data:any, config: AxiosRequestConfig) {
  return await instance.post(endPoint, data, config)
}

export async function patchData(endPoint:string, data:any) {
  return await instance.patch(endPoint, data)
}