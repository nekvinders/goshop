import { createApp } from 'vue'
import vuetify from './plugins/vuetify'
import App from './App.vue'
import router from './router'
import { registerComponents } from './registerComponents'

import BalmUI from 'balm-ui' // Official Google Material Components
import BalmUIPlus from 'balm-ui-plus' // BalmJS Team Material Components
import 'balm-ui-css'

const app = createApp(App).use(router).use(vuetify)
app.use(BalmUI)
app.use(BalmUIPlus)
registerComponents(app)

app.mount('#app')
