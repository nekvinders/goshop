module.exports = {
  transpileDependencies: ["vuetify"],
  // publicPath: process.env.NODE_ENV === 'production'
  //   ? '/' + process.env.CI_PROJECT_NAME + '/'
  //   : '/',
  // runtimeCompiler: true,
  configureWebpack: {
    resolve: {
      alias: {
        'balm-ui-plus': 'balm-ui/dist/balm-ui-plus.js',
        'balm-ui-css': 'balm-ui/dist/balm-ui.css'
      }
    }
  }
};
